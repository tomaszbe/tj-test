const fs = require('fs')

function writeCsv(filename, data) {
  const headers = Object.keys(data[0])
  const lines = data
    .map(row => {
      const mappedRow = []

      for (header of headers) {
        mappedRow.push(row[header])
      }

      return mappedRow.join(',')
    })
    .join('\n')

  const fileContents = headers.join(',') + '\n' + lines
  fs.writeFileSync(filename, fileContents)
}

module.exports = writeCsv
