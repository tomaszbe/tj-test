module.exports = ({ location, from, to }) => {
  return {
    /** Pickup date. */
    puYear: from.year(),
    puMonth: from.month() + 1,
    puDay: from.date(),
    puHour: 12,
    puMinute: 00,

    /** Drop date. */
    doYear: to.year(),
    doMonth: to.month() + 1,
    doDay: to.date(),
    doHour: 12,
    doMinute: 00,

    /** Location */
    dropFtsLocationSearch: location.id,
    ftsLocationSearch: location.id,
    dropFtsEntry: location.key,
    ftsEntry: location.key,

    /** Driver's age */
    driverage: 'on',
    driversAge: '30',

    /** Show all offers and order by price */
    ordering: 'price#0',
    showAllCars: true,

    /** This means 'Airport'. Lol. */
    ftsSearch: 'L',
    ftsType: 'A'

    /** These look irrelevant */

    // dropLocation: '27636',
    // location: '27636',
    // advSearch: null,
    // city: 'Kraków+Lotnisko',
    // coordinates: '50.0773%2C19.7881',
    // country: 'Polska',
    // countryCode: null,
    // distance: '10',
    // doFiltering: 'false',
    // dropCity: 'Kraków+Lotnisko',
    // dropCoordinates: '50.0773%2C19.7881',
    // dropCountry: 'Polska',
    // dropCountryCode: null,
    // dropFtsAutocomplete: 'Kraków+Lotnisko+%28KRK%29%2C+Polska',
    // dropFtsInput: 'kraków',
    // dropFtsLocationName: 'Kraków+Lotnisko',
    // dropFtsSearch: 'L',
    // dropFtsType: 'A',
    // dropLocationName: 'Kraków+Lotnisko',
    // enabler: null,
    // exSuppliers: null,
    // filterAdditionalInfo: null,
    // filterFrom: '0',
    // filterName: 'CarCategorisationSupplierFilter',
    // filterTo: '1000',
    // fromLocChoose: 'true',
    // ftsAutocomplete: 'Kraków+Lotnisko+%28KRK%29%2C+Polska',
    // ftsInput: 'kraków',
    // ftsLocationName: 'Kraków+Lotnisko',
    // locationName: null,
    // searchType: null,
  }
}
