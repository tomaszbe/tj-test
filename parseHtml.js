const cheerio = require('cheerio')

function getPrice(el) {
  const priceText = el
    .find('.carResultRow_Price-now')
    .first()
    .text()
    .trim()
    .replace(',', '.')
    .replace('PLN', '')

  return parseFloat(priceText)
}

function getAcriss(el) {
  return el
    .find('.car-spec__table')
    .first()
    .data('sipp')
    .substring(0, 4)
}

function getCarName(el) {
  const fullText = el
    .find('.carResultRow_CarSpec > h2')
    .first()
    .text()

  const endIndex = fullText.indexOf('\n')
  return fullText.substring
    ? fullText.substring(0, endIndex - 1)
    : 'Not found :('
}

function getVendor(el) {
  return el
    .find('.carResultRow_OfferInfo_Supplier-wrap > h4')
    .first()
    .text()
}

module.exports = function parseHtml(html) {
  const $ = cheerio.load(html)

  const rows = $('.carResultRow')
    .map(function() {
      const price = getPrice($(this))
      const acriss = getAcriss($(this))
      const carName = getCarName($(this))
      const vendor = getVendor($(this))
      return {
        price,
        acriss,
        carName,
        vendor
      }
    })
    .get()

  return rows
}
