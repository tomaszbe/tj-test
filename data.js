const moment = require('moment')

const locations = [
  { name: 'Balice', id: 27636, key: 1472594 },
  // { name: 'Pyrzowice', id: 27626, key: 1472593 },
  // { name: 'Okęcie', id: 27691, key: 1472600 },
  // { name: 'Modlin', id: 93353, key: 1472601 },
  // { name: 'Gdańsk', id: 27611, key: 1472592 },
  // { name: 'Wrocław', id: 27701, key: 1472602 },
  // { name: 'Poznań', id: 27666, key: 1472597 },
  // { name: 'Szczecin', id: 27681, key: 1472599 },
  // { name: 'Rzeszów', id: 91818, key: 1472598 },
  // { name: 'Lublin', id: 104338, key: 1472596 }
]

const durations = [1, 2, /* 3, 4, 5, 6, 7, 14, 21 */]

const dayAheads = [1, 2, /* 3, 4, 5, 6, 7, 15, 30, 60, 90 */]

function* paramSets() {
  for (location of locations) {
    for (duration of durations) {
      for (dayAhead of dayAheads) {
        yield {
          location,
          from: moment().add(dayAhead, 'days'),
          to: moment().add(dayAhead + duration, 'days')
        }
      }
    }
  }
}

function createParamSet(location, dayAhead, duration) {
  return {
    location,
    from: moment().add(dayAhead, 'days'),
    to: moment().add(dayAhead + duration, 'days')
  }
}

module.exports = {
  locations, durations, dayAheads, paramSets, createParamSet
}
