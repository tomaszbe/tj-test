const axios = require('axios')
const params = require('./params')
const parseHtml = require('./parseHtml')
const writeCsv = require('./writeCsv')

axios.defaults.baseURL = 'https://www.rentalcars.com'

const { locations, durations, dayAheads, createParamSet } = require('./data')

let counter = 0

for (location of locations) {
  const promises = []
  const promiseParams = []

  for (dayAhead of dayAheads) {
    for (duration of durations) {
      const axiosPromise = axios.get('SearchResults.do', {
        params: params(createParamSet(location, dayAhead, duration))
      })
      promises.push(axiosPromise)
      promiseParams.push({ dayAhead, duration })
    }
  }

  Promise.all(promises).then(responses => {
    const parsed = []
    responses.forEach(response => {
      parseHtml(response.data).forEach((item, index) => {
        parsed.push({
          location: location.name,
          duration: promiseParams[index].duration,
          dayAhead: promiseParams[index].dayAhead,
          ...item
        })
      })
    })
    writeCsv('out/' + location.name + '.csv', parsed)
  })
}
